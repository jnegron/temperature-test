"use strict";

var debug  = true;
var digits = ( debug ) ? 2 : 0;

var socket           = io();
var valueUpdateBound = false;
var services         = {
  electricity : {
    units : 'Kwh',
    alert : 0,
    gauge : ''
  },
  water       : {
    units : 'Lts',
    alert : 0,
    gauge : ''
  },
  gas         : {
    units : 'Kg',
    alert : 0,
    gauge : ''
  },
};

(function( $ ) {
  angular.module( 'Prolum' ).controller( 'IndexController', function( $scope, $interval ) {
    $scope.$on( '$stateChangeSuccess', function() {
      socket.removeAllListeners( 'valueUpdate' );
      socket.removeAllListeners( 'config:set:electricity' );
      socket.removeAllListeners( 'config:set:water' );
      socket.removeAllListeners( 'config:set:gas' );
      socket.removeAllListeners( 'config:set:last' );

      // Initializes Gauges
      initGauges();

      updateValuesFromDatabase();

      gaugeScroll();

      // @ToDo: Add the other math operations.
      socket.on( 'valueUpdate', updateData );
      socket.on( 'config:set:electricity', updateDataFromConfig );
      socket.on( 'config:set:water', updateDataFromConfig );
      socket.on( 'config:set:gas', updateGasFromConfig );
      socket.on( 'values:set:last', updateCurrentValues );
    } );
  } );

  
  function updateCurrentValues( data ) {
    // console.log( data );
    var service = $( '#' + data.service + '-service' );

    service.find( '.average-day' ).text( data.average.value.toFixed( digits ) + ' ' + services[ data.service ].units );
    service.find( '.total' ).text( data.total.value.toFixed( digits ) + ' ' + services[ data.service ].units );
    service.find( '.expense' ).text( '$' + data.expense.value.toFixed( digits ) );

    setStatus( data, '.average-day', data.average.status );
    setStatus( data, '.total', data.total.status );
    setStatus( data, '.expense', data.expense.status );
  }

  function updateData( data ) {
    //console.log( data );
    var service = $( '#' + data.service + '-service' );

    // Changes values.
    service.find( '.current' ).text( data.value.toFixed( digits ) + ' ' + data.units );
    service.find( '.average-day' ).text( data.average.value.toFixed( digits ) + ' ' + services[ data.service ].units );
    service.find( '.total' ).text( data.total.value.toFixed( digits ) + ' ' + services[ data.service ].units );
    service.find( '.expense' ).text( '$' + data.expense.value.toFixed( digits ) );

    // Sets Status for each value.
    setStatus( data, '.current', data.status );
    setStatus( data, '.average-day', data.average.status );
    setStatus( data, '.total', data.total.status );
    setStatus( data, '.expense', data.expense.status );

    services[ data.service ].gauge.arrows[ 0 ].setValue( Math.round( data.value ) );
    
    if(data.status == 'warning' && services[data.service].alert == 0){
      services[data.service].alert = 1;
      alert("ADVERTENCIA \nesta excediendo su consumo de "+ serviceName(data.service));
      services[data.service].alert = 0;
    }
  }
  function serviceName(service){
    if(service == 'water') return 'AGUA';
    if(service == 'electricity') return 'ELECTRICIDAD';
    if(service == 'gas') return 'GAS';
  }
  function updateValuesFromDatabase() {
    for ( var service in services ) {
      //console.log( service );
      if ( service == 'gas' ) {
        socket.emit( 'config:get:gas' );
      } else {
        //console.log( 'config service:', service );
        socket.emit( 'config:get:' + service );
      }

      socket.emit( 'values:get:last', { service : service } );
    }
  }

  function updateDataFromConfig( config ) {
    if ( config.service == 'gas' ) {
      return;
    }

    var service = $( '#' + config.service + '-service' );
    var expense = ( '' == config.expense ) ? 0 : Number( config.expense );

    if ( 'water' == config.service ) {
      //console.log( config );
    }
    service.find( '.target' ).text( Math.round( config.average ) + ' ' + services[ config.service ].units );
    service.find( '.target-expense' ).text( '$' + expense.toFixed( digits ) );
  }

  function updateGasFromConfig( data ) {
    var service = $( '#gas-service' );
    service.find( '.target-expense' ).text( '$' + data.cost.toFixed( 2 ) );

    // Changes Tank max capacity.
    services.gas.gauge.axes[ 0 ].endValue = data.quantity;

    // Changes Bands.
    services.gas.gauge.axes[ 0 ].bands[ 0 ].endValue = data.quantity * 0.15;

    services.gas.gauge.axes[ 0 ].bands[ 1 ].startValue = data.quantity * 0.15;
    services.gas.gauge.axes[ 0 ].bands[ 1 ].endValue   = data.quantity * 0.5;

    services.gas.gauge.axes[ 0 ].bands[ 2 ].startValue = data.quantity * 0.5;
    services.gas.gauge.axes[ 0 ].bands[ 2 ].endValue   = data.quantity;

    services.gas.gauge.arrows[ 0 ].setValue( data.current.toFixed( 0 ) );

    service.find( '.current' ).text( data.current.toFixed( digits ) + ' Kg' );
    service.find( '.target' ).text( data.average.toFixed( digits ) + ' Kg' );
    // service.find( '.total' ).text( data.total.toFixed( 0 ) + ' Kg' );

    services.gas.gauge.validateNow();
  }

  function setStatus( data, selector, status ) {
    $( '#' + data.service + '-service' ).find( selector ).removeClass( 'warning' ).removeClass( 'ok' ).removeClass( 'caution' ).addClass( status );
  }

  function initGauges() {
    services.electricity.gauge = elecGauge();
    services.water.gauge       = waterGauge();
    services.gas.gauge         = gasGauge();

    cleanTexts();

    // Sets gauges as carousels when in Raspberry View.
    if ( $( window ).width() <= 600 ) {
      var i = 0;
      $( '.service' ).each( function() {
        $( this ).css( 'left', 720 * i );
        i ++;
      } );
    }
  }

  //@ToDo: Add animations to the scrolls.
  function gaugeScroll() {
    if ( $( window ).width() <= 600 ) {
      var i      = 0;
      var offset = 720;

      hideSwitchButtons();

      $( '.switch-button.right' ).click( function() {
        $( '.service' ).each( function() {
          var index = $( this ).attr( 'data-index' );
          index --;

          var left = index * offset;

          $( this ).css( 'left', left );
          $( this ).attr( 'data-index', index );
        } );

        hideSwitchButtons();
      } );

      $( '.switch-button.left' ).click( function() {
        $( '.service' ).each( function() {
          var index = $( this ).attr( 'data-index' );
          index ++;

          var left = index * offset;

          $( this ).css( 'left', left );
          $( this ).attr( 'data-index', index );
        } );

        hideSwitchButtons();
      } );
    }
  }

  function hideSwitchButtons() {
    if ( $( '.service:first' ).attr( 'data-index' ) == 0 ) {
      $( '.switch-button.left' ).css( 'display', 'none' );
    } else {
      $( '.switch-button.left' ).css( 'display', 'block' );
    }

    if ( $( '.service:last' ).attr( 'data-index' ) == 0 ) {
      $( '.switch-button.right' ).css( 'display', 'none' );
    } else {
      $( '.switch-button.right' ).css( 'display', 'block' );
    }
  }

  function cleanTexts() {
    var href = 'http://www.amcharts.com/javascript-charts/';
    $( 'a[href="' + href + '"]' ).css( 'display', 'none' );
  }

  function waterGauge() {
    return AmCharts.makeChart( 'water', {
      type          : 'gauge',
      startDuration : 1,
      //fontSize      : 15,
      theme         : 'dark',
      titles        : [
        {
          id   : 'water-title',
          size : 14,
          text : 'Agua'
        }
      ],
      arrows        : [
        {
          id    : 'water-arrow',
          value : 0,
          "borderAlpha": 0,
          "innerRadius": "0%",
          "nailBorderThickness": 0,
          "nailRadius": 0,
          "radius": "100%",
          "startWidth": 5
        }
      ],
      axes          : [
        {
          axisThickness      : 1,
          bottomText         : 'L/h',
          bottomTextFontSize : 11,
          //bottomTextYOffset  : - 50,
          endValue           : 800,
          id                 : 'water-axis',
          valueInterval      : 200,
          //fontSize           : 11,
          //labelOffset        : 25,
          bands              : [
            {
              alpha       : 0.7,
              color       : '#00CC68',
              startValue  : 0,
              endValue    : 400,
              id          : 'water-band-1',
              innerRadius : '80%'
            },
            {
              alpha       : 0.7,
              color       : '#ffac29',
              startValue  : 400,
              endValue    : 600,
              id          : 'water-band-2',
              innerRadius : '80%'
            },
            {
              alpha       : 0.7,
              color       : '#ea3838',
              startValue  : 600,
              endValue    : 800,
              id          : 'water-band-3',
              innerRadius : '80%'
            },
          ]
        }
      ]
    } );
  }

  function gasGauge() {
    return AmCharts.makeChart( 'gas', {
      type          : 'gauge',
      startDuration : 1,
      //fontSize      : 15,
      theme         : 'dark',
      titles        : [
        {
          id   : 'gas-title',
          size : 14,
          text : 'Gas'
        }
      ],
      arrows        : [
        {
          id    : 'gas-arrow',
          value : 0,
          "borderAlpha": 0,
          "innerRadius": "0%",
          "nailBorderThickness": 0,
          "nailRadius": 0,
          "radius": "100%",
          "startWidth": 5
        }
      ],
      axes          : [
        {
          axisThickness      : 1,
          bottomText         : 'Kg',
          bottomTextFontSize : 11,
          //bottomTextYOffset  : - 50,
          endValue           : 20,
          id                 : 'gas-axis',
          valueInterval      : 5,
          //fontSize           : 18,
          //labelOffset        : 25,
          bands              : [
            {
              alpha       : 0.7,
              color       : '#ea3838',
              startValue  : 0,
              endValue    : 3,
              id          : 'gas-band-1',
              innerRadius : '80%'
            },
            {
              alpha       : 0.7,
              color       : '#ffac29',
              startValue  : 3,
              endValue    : 10,
              id          : 'gas-band-2',
              innerRadius : '80%'
            },
            {
              alpha       : 0.7,
              color       : '#00CC68',
              startValue  : 10,
              endValue    : 20,
              id          : 'gas-band-3',
              innerRadius : '80%'
            },
          ]
        }
      ]
    } );
  }

  function elecGauge() {
    return AmCharts.makeChart( 'electricity', {
      type          : 'gauge',
      startDuration : 1,
      fontSize      : 9,
      theme         : 'dark',
      titles        : [
        {
          id   : 'electricity-title',
          size : 14,
          text : 'Luz'
        }
      ],
      arrows        : [
        {
          id    : 'electricity-arrow',
          value : 0,
          "borderAlpha": 0,
          "innerRadius": "0%",
          "nailBorderThickness": 0,
          "nailRadius": 0,
          "radius": "100%",
          "startWidth": 5
        }
      ],
      axes          : [
        {
          axisThickness      : 1,
          bottomText         : 'w/h',
          bottomTextFontSize : 11,
          //bottomTextYOffset  : - 50,
          endValue           : 3000,
          id                 : 'electricity-axis',
          valueInterval      : 1500,
          //fontSize           : 18,
          //labelOffset        : 25,
          bands              : [
            {
              alpha       : 0.7,
              color       : '#00CC68',
              startValue  : 0,
              endValue    : 750,
              id          : 'electricity-band-1',
              innerRadius : '80%'
            },
            {
              alpha       : 0.7,
              color       : '#ffac29',
              startValue  : 750,
              endValue    : 2250,
              id          : 'electricity-band-2',
              innerRadius : '80%'
            },
            {
              alpha       : 0.7,
              color       : '#ea3838',
              startValue  : 2250,
              endValue    : 3000,
              id          : 'electricity-band-3',
              innerRadius : '80%'
            },
          ]
        }
      ]
    } );
  }

  function gauge( id ) {
    return AmCharts.makeChart( id, {
      type          : 'gauge',
      startDuration : 1,
      //fontSize      : 15,
      theme         : 'dark',
      titles        : [
        {
          id   : id + '-title',
          size : 28,
          text : 'Luz'
        }
      ],
      arrows        : [
        {
          id    : id + '-arrow',
          value : 750
        }
      ],
      axes          : [
        {
          axisThickness      : 1,
          bottomText         : 'Kw/h',
          bottomTextFontSize : 11,
          //bottomTextYOffset  : - 50,
          endValue           : 3,
          id                 : id + '-axis',
          valueInterval      : .3,
          //fontSize           : 18,
          //labelOffset        : 40,
          bands              : [
            {
              alpha       : 0.7,
              color       : '#00CC00',
              startValue  : 0,
              endValue    : 750,
              id          : id + '-band-1',
              innerRadius : '80%'
            },
            {
              alpha       : 0.7,
              color       : '#ffac29',
              startValue  : 750,
              endValue    : 2250,
              id          : id + '-band-2',
              innerRadius : '80%'
            },
            {
              alpha       : 0.7,
              color       : '#ea3838',
              startValue  : 2250,
              endValue    : 3000,
              id          : id + '-band-3',
              innerRadius : '80%'
            },
          ]
        }
      ]
    } );
  }

  function getRandomInt( min, max ) {
    return Math.floor( Math.random() * (max - min + 1) ) + min;
  }

})( jQuery );

