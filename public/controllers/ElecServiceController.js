"use strict";

angular.module( 'Prolum' ).controller( 'ElecServiceController', function( $scope, $location ) {
  $scope.config  = {};
  $scope.service = 'electricity';

  $scope.$on( '$viewContentLoaded', function() {
    socket.removeAllListeners( 'config:set:electricity' );
    socket.removeAllListeners( 'config:update:electricity:success' );

    socket.emit( 'config:get:electricity' );

    socket.on( 'config:set:electricity', elecConfig );

    socket.on( 'config:update:electricity:success', function() {
      alert( 'La configuración se guardo con exito!' );
      $location.path( 'index' );
      $scope.$apply();
    } );
  } );

  $scope.update = function( config ) {
    $scope.config           = angular.copy( config );
    $scope.config.bimonthly = ( $scope.config.bimonthly ) ? true : false;

    socket.emit( 'config:update:electricity', $scope.config );
  };

  function elecConfig( options ) {
    $scope.config = {
      expense   : options.expense,
      prices    : options.prices,
      date      : options.date,
      bimonthly : ( options.bimonthly == "true" ) ? true : false,
      service   : $scope.service
    }
    //console.log( $scope.config );

    $scope.$apply( $scope.config );
  }

  $scope.change = function() {
    if ( $scope.config.date == '' ) {
      $scope.config.date = 1;
    }

    if ( Number( $scope.config.date ) <= 0 ) {
      $scope.config.date = 1;
    }

    if ( Number( $scope.config.date ) >= 31 ) {
      $scope.config.date = 31;
    }

    if ( $scope.config.expense == '' ) {
      $scope.config.expense = 0;
    }

    for ( var i = 0; i < $scope.config.prices.length; i++ ) {
      //console.log( $scope.config.prices[ i ] );
      if ( $scope.config.prices[ i ] == '' ) {
        $scope.config.prices[ i ] = 0;
      }
    }
  }
} );
