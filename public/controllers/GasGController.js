"use strict";
var debug  = true;
var digits = ( debug ) ? 2 : 0;

var socket           = io();

(function($){
  angular.module('Prolum').controller('GasGController', function( $scope, $location ){
    $scope.expense = 500;
    $scope.$on( '$stateChangeSuccess', function() {
      socket.emit('config:get:gas');
      socket.emit('prueba', {servicio : 'gas'});
      socket.on('config:set:gas', set_expense_data);
      socket.on('news', graficar);

    });

    function set_expense_data(data){
      //console.log(data);
      //var data_aux = data[0]['cost'];
      //var data_int = parseInt(data[0]['cost']).toFixed(2);
      $('.expense').text('$' + parseInt(data.cost).toFixed(2));
      $('.date').text(data.quantity + ' Kg');
    }
    function graficar(data){
    var anios = [];
    var bimestres = ['B1', 'B2', 'B3', 'B4', 'B5', 'B6'];
    var cont = 0;

    if(data.length > 0){
    var y_now = new Date(data[0].created_at);
    anios.push(y_now.getFullYear());
    
    for(var i = 0; i < data.length; i++){      
      var y_aux = new Date(data[i].created_at);
      if(y_aux.getFullYear() != y_now.getFullYear()){
        y_now = y_aux;
        anios.push(y_now.getFullYear());
        
      }   
    }
    var gastos = [];
    for(var i = 0; i < anios.length; i++){
      for(var j = 0; j < bimestres.length; j++){
        gastos[(i*6)+j] = {anio : anios[i], bimestre : bimestres[j], gasto : 0};
      }
    }

    for(var i = 0; i< gastos.length; i++){
      for(var j = 0; j < data.length; j++){
        var y_aux = new Date(data[j].created_at);
        if((gastos[i].anio == y_aux.getFullYear()) && (gastos[i].bimestre == getBimestre(y_aux.getMonth()))){
          gastos[i].gasto += data[j].expense;
        }
      }
    }
    
    var datos = [];

    for(var i = 0; i < anios.length; i++){
      var filas = [];
      var series = {};
      for(var k = 0; k < bimestres.length; k++){
        filas.push(gastos[(i*6) + k].gasto);
      }
      series['name'] = anios[i];
      series['data'] = filas;
      datos.push(series);
    }
    }else{
      datos = [{data : [0,0,0,0,0,0], name : 'Sin datos'}];
    }
    
    function getBimestre(data){
      switch (data) {
        case 0 :
          return "B1";
        break;
        case 1 :
          return "B1";
        break;
        case 2 :
          return "B2";
        break;
        case 3 :
          return "B2";
        break;
        case 4 :
          return "B3";
        break;
        case 5 :
          return "B3";
        break;
        case 6 :
          return "B4";
        break;
        case 7 :
          return "B4";
        break;
        case 8 :
          return "B5";
        break;
        case 9 :
          return "B5";
        break;
        case 10 :
          return "B6";
        break;
        case 11:
          return "B6";
        break;
      }
    }

    $(function () {
        $('#Grap').highcharts({
            colors: ["#293389", "#bc3838", "#29892c", "#d6b600", "#aaeeee", "#ff0066", "#eeaaee", "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
            title: {
             style : {
              color : '#FFFFFF'
             },
             text: 'Comparativo Gasto Bimestral'
            },
            labels : {
              style : {
                color : '#FFFFFF'
              }
            },
            chart: {
                backgroundColor: 'transparent',
                type: 'column'
            },
            xAxis: {
              categories: ['B1', 'B2', 'B3', 'B4', 'B5', 'B6'],
              labels  : {
                style : {
                  color : '#FFFFFF',
                  size  : 'large' 
                }
              }
            },
            yAxis: {
              labels : {
                format : '${value}',
                style  : {
                  color : '#FFFFFF'
                }
              }
            },
            legend: {
                backgroundColor: '#FFFFFF'
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.x + ': ' + this.y;
                }
            },
            plotOptions: {
              series: {
                borderColor: '#000000'
              }
            },
            series: datos
        });
    });
    
  }

    
  });
})( jQuery );
