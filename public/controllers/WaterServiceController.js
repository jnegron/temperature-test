"use strict";
var waterConfigBound = false;
var bound            = false;
(function($){
angular.module( 'Prolum' ).controller( 'WaterServiceController', function( $scope, $location ) {
  $scope.config  = {};
  $scope.service = 'water';
 
  $scope.$on( '$viewContentLoaded', function() {
    socket.removeAllListeners( 'config:set:water' );
    socket.removeAllListeners( 'config:update:water:success' );

    socket.emit( 'config:get:water', { service : $scope.service } );

    socket.on( 'config:set:water', function( options ) {
      $scope.config = {
        expense   : Number( options.expense ),
        prices    : ( options.prices != 0 || options.prices != '' ) ? options.prices : 'D1',
        date      : options.date,
        bimonthly : ( options.bimonthly == "true" ) ? true : false,
        service   : $scope.service
      }
      $scope.$apply();
    } );

    socket.on( 'config:update:water:success', function() {
      alert( 'La configuración se guardo con exito!' );
      $location.path( 'index' );
      $scope.$apply();
    } );
  } );

  $scope.update = function( config ) {
    $scope.config           = angular.copy( config );
    $scope.config.bimonthly = ( $scope.config.bimonthly ) ? true : false;

    // console.log( $scope.config );
    socket.emit( 'config:update:water', $scope.config );
  };

  $scope.change = function() {
    if ( $scope.config.expense == '' ) {
      $scope.config.expense = 0;
    }

    if ( $scope.config.date == '' ) {
      $scope.config.date = 1;
    }

    if ( Number( $scope.config.date ) <= 0 ) {
      $scope.config.date = 1;
    }

    if ( Number( $scope.config.date ) >= 31 ) {
      $scope.config.date = 31;
    }
  }
} );
})( jQuery );
