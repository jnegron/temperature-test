"use strict";
var gasConfigBound = false;

angular.module( 'Prolum' ).controller( 'GasServiceController', function( $location, $scope ) {
  $scope.tank    = {};
  $scope.service = 'gas';

  $scope.$on( '$viewContentLoaded', function() {
    socket.removeAllListeners( 'config:set:gas' );
    socket.removeAllListeners( 'config:update:gas:success' );

    socket.emit( 'config:get:gas' );

    socket.on( 'config:set:gas', function( options ) {
      //console.log( options );

      $scope.config = {
        service  : $scope.service,
        quantity : options.quantity.toFixed( 0 ),
        expense  : options.cost.toFixed( 2 )
      }

      //console.log( $scope.config );

      $scope.$apply();
    } );

    socket.on( 'config:update:gas:success', function() {
      alert( 'La configuración se guardo con exito!' );
      $location.path( 'index' );
      $scope.$apply();
    } );
  } );

  $scope.update = function( config ) {
    $scope.config = {
      quantity : parseFloat( config.quantity ),
      price    : parseFloat( config.expense )
    };

    //console.log( $scope.config );
    socket.emit( 'config:update:gas', $scope.config );
  };
  
  $scope.change = function() {
    if ( $scope.config.expense == '' ) {
      $scope.config.expense = 0;
    }
  }
} );
