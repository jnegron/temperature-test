var isOn = false;
var socket = io();

$(document).ready(function(){
  $('.mdl-spinner.mdl-js-spinner.is-active').show();
  $('#temp').hide();
  socket.on('datos', function (data) {
    console.log(chart);
    //console.log(typeof $('#container').highcharts.Series);
	var x = (new Date()).getTime(), // current time
		y = parseFloat(data);
    chart.series[0].addPoint([x, y], true, true);
    $('.mdl-spinner.mdl-js-spinner.is-active').hide();
    $('#temp').show();
    document.getElementById("temp").innerHTML = data.toString()+" °C";
  });
});



  var chart = Highcharts.chart('container', {
      chart: {
          type: 'spline',
          animation: Highcharts.svg, // don't animate in old IE
          marginRight: 10,
          events: {
            load: function () {

              
            }
        }

          
      },
      title: {
          text: 'Temperatura'
      },
      xAxis: {
          type: 'datetime',
          tickPixelInterval: 150
      },
      yAxis: {
          title: {
              text: 'Value'
          },
          plotLines: [{
              value: 0,
              width: 1,
              color: '#808080'
          }]
      },
      
      tooltip: {
          formatter: function () {
              return '<b>' + this.series.name + '</b><br/>' +
                  Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                  Highcharts.numberFormat(this.y, 2);
          }
      },
      
      legend: {
          enabled: false
      },
      exporting: {
          enabled: false
      },
      series: [{
          name: 'Temperatura',
          data: (function () {
              // generate an array of random data
              var data = [],
                  time = (new Date()).getTime(),
                  i;

              for (i = -19; i <= 0; i += 1) {
                  data.push({
                      x: time + i * 1000,
                      y: 0
                  });
              }
              return data;
          }())
      }]
  });

  Highcharts.setOptions({
    global: {
        useUTC: false
    }
  });