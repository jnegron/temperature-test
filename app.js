var http    = require('http');
var express = require('express');
var request = require("request");
var path    = require('path');
var app     = express();
var io      = require('socket.io')(app.listen(3000));
var serialport = require('serialport');
/*
var config = {
  baudRate: 9600,
  autoOpen:false
}
*/
//var port = new serialport('/dev/ttyACM0', config);

app.set('views', path.join( __dirname, 'views'));
app.set('view engine', 'jade');

app.use( '/modules', express.static( path.join( __dirname, 'node_modules' ) ) );
app.use( express.static( path.join( __dirname, 'public' ) ) );

app.get('/', function(req, res){
  res.render('index');
})
app.get('/add.html', function(req, res){
  res.render('index');
})

function getRandomArbitrary(min, max) {
  number =  Math.random() * (max - min) + min;
   return Math.round( number * 10 ) / 10;
}

io.sockets.on('connection', function (socket) {
  setInterval(function(){
    socket.emit('datos', getRandomArbitrary(20, 30));
  }, 1000);
  socket.emit('hola', getRandomArbitrary(20, 30));
  //}, 1000);
});
/**
port.open(function (err) {
  if (err) { return console.log('Error opening port: ', err.message);}
  console.log('Listo para leer el puerto');
});


io.sockets.on('connection', function (socket) {
  port.on('data', function(data) {
    try {
      body = JSON.parse(data);
      console.log(body);
      socket.emit('datos', body['temp']);
    } catch (e) {
      return console.error(e);
    } 
  }); 
});

*/